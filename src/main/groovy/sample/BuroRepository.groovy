package sample

import org.springframework.data.repository.CrudRepository

interface BuroRepository extends CrudRepository<BuroCache, String> {
}
