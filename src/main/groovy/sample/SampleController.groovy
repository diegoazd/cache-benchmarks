package sample

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class SampleController {

    @Autowired
    BuroRepository buroRepository

    @GetMapping("/foo")
    String foo() {
        println "foooo"
        "test"
    }

    @GetMapping("/id/{id}")
    String find(@PathVariable String id) {
        buroRepository.findOne(id).burFol
    }
}
