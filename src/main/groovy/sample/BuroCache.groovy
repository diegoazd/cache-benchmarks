package sample

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="ln_mx_buro_cache")
class BuroCache {

    @Id
    String mx_solicitud_buro;
    @Column(name="mx_folio")
    String mx_folio;
    @Column(name="idProvider")
    String idProvider;
    @Column(name="fecConsult")
    Date fecConsult;
    @Column(name="calificacion_kubo")
    String calificacion_kubo;
    @Column(name="kubo_score_class")
    String kubo_score_class;
    @Column(name="nombreCompleto")
    String nombreCompleto;
    @Column
    String rfc;
    @Column
    String consultas_propias;
    @Column(name="consultas_propiasMS")
    String consultas_propiasMS;
    @Column(name="consultas_propiasULT")
    String consultas_propiasULT;
    @Column(name="consultasOtras")
    String consultasOtras;
    @Column(name="consultasOtrasMS")
    String consultasOtrasMS;
    @Column(name="consultasOtrasULT")
    String consultasOtrasULT;
    @Column(name="consultasKubo")
    String consultasKubo;
    @Column(name="consultasKuboULT")
    String consultasKuboULT;
    @Column(name="consultasKuboMS")
    String consultasKuboMS;
    @Column(name="totalConsult")
    String totalConsult;
    @Column(name="tableRulesStr")
    String tableRulesStr;
    @Column(name="tableAlertas1Str")
    String tableAlertas1Str;
    @Column(name="tableAlertas2Str")
    String tableAlertas2Str;
    @Column(name="tableConsultUltData")
    String tableConsultUltData;
    @Column(name="tableConsultMSData")
    String tableConsultMSData;
    @Column(name="tableVigData")
    String tableVigData;
    @Column(name="tableVigDataCom")
    String tableVigDataCom;
    @Column(name="tableDomicilios")
    String tableDomicilios;
    @Column(name="tableCredCerr")
    String tableCredCerr;
    @Column(name="tableCredCerr_M_6")
    String tableCredCerr_M_6;
    @Column(name="chartModel")
    String chartModel;
    @Column(name="maxGrid")
    String maxGrid;
    @Column(name="scriptJSon")
    String scriptJSon;
    @Column(name="scriptSaldos")
    String scriptSaldos;
    @Column(name="prospectus_id")
    String prospectus_id;
    @Column(name="nombre1")
    String nombre1;
    @Column(name="nombre2")
    String nombre2;
    @Column(name="apellido1")
    String apellido1;
    @Column(name="apellido2")
    String apellido2;
    @Column(name="kubo_score_letter")
    String kubo_score_letter;
    @Column(name="kubo_score_number")
    String kubo_score_number;
    @Column(name="score_buro")
    String score_buro;

    @Column(name="burFol")
    String burFol;
    @Column(name="tipoConsulta")
    String tipoConsulta;
    @Column(name="scoreIcc")
    String scoreIcc;

    @Column(name="numsols")
    String numsols;
    @Column(name="cta_cgarantia")
    String cta_cgarantia;
    @Column(name="quebrantos")
    String quebrantos;

    @Column(name="fraudes")
    String fraudes;
    @Column(name="roboidentidad")
    String roboidentidad;
    @Column(name="fraudesnoatribuible")
    String fraudesnoatribuible;
    @Column(name="fianzas")
    String fianzas;
    @Column(name="aplicogarantia")
    String aplicogarantia;
    @Column(name="nolocalizable")
    String nolocalizable;
    @Column(name="codemandado")
    String codemandado;
    @Column(name="condonaciones")
    String condonaciones;
    @Column(name="reestructura")
    String reestructura;
    @Column(name="fec_credantiguo")
    Date fec_credantiguo;
    @Column(name="strCreditos")
    String strCreditos;
    @Column(name="limiteagregado")
    String limiteagregado;
    @Column(name="saldoagregado")
    String saldoagregado;
    @Column(name="montoagregado")
    String montoagregado;
    @Column(name="prim_credito")
    Date prim_credito;
    @Column(name="ult_credito")
    Date ult_credito;
    @Column(name="max_liquidado")
    String max_liquidado;
    @Column(name="max_noliquidado")
    String max_noliquidado;

    @Column(name="numCorriente")
    String numCorriente;
    @Column(name="saldoCorriente")
    String saldoCorriente;
    @Column(name="montoAPagarCorriente")
    String montoAPagarCorriente;
    @Column(name="pagoPeriodicoCorriente")
    String pagoPeriodicoCorriente;
    @Column(name="saldoVencidoCorriente")
    String saldoVencidoCorriente;
    @Column(name="maxCredCorriente")
    String maxCredCorriente;
    @Column(name="limiteMaxCorriente")
    String limiteMaxCorriente;
    @Column(name="numAtraso")
    String numAtraso;
    @Column(name="saldoAtraso")
    String saldoAtraso;
    @Column(name="montoAPagarAtraso")
    String montoAPagarAtraso;
    @Column(name="pagoPeriodicoAtrasado")
    String pagoPeriodicoAtrasado;
    @Column(name="maxCredAtraso")
    String maxCredAtraso;
    @Column(name="limiteMaxAtrasado")
    String limiteMaxAtrasado;
    @Column(name="saldoVencidoAtrasado")
    String saldoVencidoAtrasado;
}
